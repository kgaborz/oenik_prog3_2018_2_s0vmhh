﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.ILogic
{
    using System;
    using System.Net;
    using System.Text;
    using CarShop.Repository;

    /// <summary>
    /// Logic class, which implements the <see cref="ILogicI"/> interface and all the CRUD methods from <see cref="IRepository"/>.
    /// </summary>
    public class Logic : ILogicI
    {
        private static IRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// Constructor.
        /// </summary>
        public Logic()
        {
            repo = new RepositoryC();
        }

        /// <summary>
        /// Makes the repository object call the Create method.
        /// </summary>
        /// <param name="table">Defines the table's name where the record should be created.</param>
        /// <param name="data">The record's parameter.</param>
        public void Create(string table, string data)
        {
            switch (table)
            {
                case "Brand":
                    repo.CreateBrand(data);
                    break;
                case "Car":
                    repo.CreateCar(data);
                    break;
                case "Extra":
                    repo.CreateExtra(data);
                    break;
                case "ModelExtraSwitch":
                    repo.CreateModelExtraSwitch(data);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Makes the repository object call the Delete method.
        /// </summary>
        /// <param name="table">Defines the table's name where the record should be deleted.</param>
        /// <param name="id">ID, which indicates to the data that needs to be deleted.</param>
        public void Delete(string table, string id)
        {
            switch (table)
            {
                case "Brand":
                    repo.DeleteBrand(id);
                    break;
                case "Car":
                    repo.DeleteCar(id);
                    break;
                case "Extra":
                    repo.DeleteExtra(id);
                    break;
                case "ModelExtraSwitch":
                    repo.DeleteModelExtraSwitch(id);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Makes the repository object call the Read method.
        /// </summary>
        /// <param name="table">Defines the table's name that should be read.</param>
        public void Read(string table)
        {
            switch (table)
            {
                case "Brand":
                    repo.ReadBrand();
                    break;
                case "Car":
                    repo.ReadCar();
                    break;
                case "Extra":
                    repo.ReadExtra();
                    break;
                case "ModelExtraSwitch":
                    repo.ReadModelExtraSwitch();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Makes the repository object call the Update method.
        /// </summary>
        /// <param name="table">Defines the table's name where the record should be updated.</param>
        /// <param name="id">ID, which indicates to the data that needs to be updated.</param>
        /// <param name="data">The record's parameter.</param>
        public void Update(string table, string id, string data)
        {
            switch (table)
            {
                case "Brand":
                    repo.UpdateBrand(id, data);
                    break;
                case "Car":
                    repo.UpdateCar(id, data);
                    break;
                case "Extra":
                    repo.UpdateExtra(id, data);
                    break;
                case "ModelExtraSwitch":
                    repo.UpdateModelExtraSwitch(id, data);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Makes the repository object count how many extras are connected to a car from each type of categories.
        /// </summary>
        public void CountCategories()
        {
            repo.CountCategories();
        }

        /// <summary>
        /// Gets a car's full price (Base price + extras).
        /// </summary>
        /// <param name="id">The car's ID which we want to know the full price of.</param>
        /// <returns>Returns the full price of the car.</returns>
        public decimal GetFullPrice(string id)
        {
             return repo.GetFullPrice(id);
        }

        /// <summary>
        /// Makes the repository object calculate the avarage price all of the cars from each brands.
        /// </summary>
        public void AvaragePrice()
        {
            repo.AvaragePrice();
        }

        /// <summary>
        /// Connects to Java application, which makes bids for a car.
        /// </summary>
        /// <param name="name">The name of the car.</param>
        /// <param name="price">The base price of the car.</param>
        public void WebInteraction(string name, string price)
        {
            using (var wb = new WebClient())
            {
                var response = wb.DownloadData("http://localhost:8080/CarShop.JavaEndpoint/XMLSender?name=" + name + "&price=" + price);
                string responsS = Encoding.UTF8.GetString(response);
                Console.WriteLine(responsS);
            }
        }
    }
}
