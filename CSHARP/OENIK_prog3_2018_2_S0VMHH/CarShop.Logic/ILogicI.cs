﻿// <copyright file="ILogicI.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.ILogic
{
    using System;
    using System.Collections.Generic;
    using CarShop.Data;

    /// <summary>
    /// The interface which defines all methods for CRUD operations.
    /// </summary>
    public interface ILogicI
    {
        /// <summary>
        /// Create method for creating a new record.
        /// </summary>
        /// <param name="table">The table's name where the record needs to be created.</param>
        /// <param name="data">The new record's data.</param>
        void Create(string table, string data);

        /// <summary>
        /// A method which reads a table's datas.
        /// </summary>
        /// <param name="table">The table's name which needs to be read.</param>
        void Read(string table);

        /// <summary>
        /// Updating method.
        /// </summary>
        /// <param name="table">The table's name where the record needs to be updated.</param>
        /// <param name ="id">The ID, which indicates to the data that is need to be changed. </param>
        /// <param name="data">The updating parameter.</param>
        void Update(string table, string id, string data);

        /// <summary>
        /// Deleting method.
        /// </summary>
        /// <param name="table">The table's name where the record needs to be deleted.</param>
        /// <param name="id">ID, which indicates to the data that is need to be deleted.</param>
        void Delete(string table, string id);

        /// <summary>
        /// Counts how many extras are connected to a car from each type of categories.
        /// </summary>
        void CountCategories();

        /// <summary>
        /// Gets a car's full price (Base price + extras).
        /// </summary>
        /// <param name="id">The car's ID which we want to know the full price of.</param>
        /// <returns>Returns the full price of the car.</returns>
        decimal GetFullPrice(string id);

        /// <summary>
        /// Get the avarage price all of the cars from each brands.
        /// </summary>
        void AvaragePrice();

        /// <summary>
        /// Connects to Java application, which makes bids for a car.
        /// </summary>
        /// <param name="name">The name of the car.</param>
        /// <param name="price">The base price of the car.</param>
        void WebInteraction(string name, string price);
    }
}
