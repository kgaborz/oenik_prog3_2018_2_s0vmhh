﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.ILogic.Tests
{
    using System.Collections.Generic;
    using CarShop.Data;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;
    using static Logic;

    /// <summary>
    /// LogicTests class includes the Logic's tests.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        /// <summary>
        /// Verifies if the Logic object calls the CreateBrand method correctly.
        /// </summary>
        [Test]
        public void BrandCreationIsDone()
        {
            // Arrange
            var logic = new Logic();
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            repoMock.Setup(x => x.CreateBrand(It.IsAny<string>())).Verifiable();

            // Act
            logic.Create("Brand", "25;Suzuki;Japan;1990;http://www.suzuki.hu;160000000;");

            // Assert
            repoMock.Verify(x => x.CreateBrand(It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Verifies if the Logic object calls the CreateCar method correctly.
        /// </summary>
        [Test]
        public void CarCreationIsDone()
        {
            // Arrange
            var logic = new Logic();
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            repoMock.Setup(x => x.CreateCar(It.IsAny<string>())).Verifiable();

            // Act
            logic.Create("Car", "25;A8;2007;1200;1000;800000;");

            // Assert
            repoMock.Verify(x => x.CreateCar(It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Verifies if the Logic object calls the CreateExtra method correctly.
        /// </summary>
        [Test]
        public void ExtraCreationIsDone()
        {
            // Arrange
            var logic = new Logic();
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            repoMock.Setup(x => x.CreateExtra(It.IsAny<string>())).Verifiable();

            // Act
            logic.Create("Extra", "25;accessory;spoiler;7000;black;1;");

            // Assert
            repoMock.Verify(x => x.CreateExtra(It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Verifies if the Logic object calls the CreateModelExtraSwitch method correctly.
        /// </summary>
        [Test]
        public void ModelExtraSwitchCreationIsDone()
        {
            // Arrange
            var logic = new Logic();
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            repoMock.Setup(x => x.CreateModelExtraSwitch(It.IsAny<string>())).Verifiable();

            // Act
            logic.Create("ModelExtraSwitch", "25;1;2;");

            // Assert
            repoMock.Verify(x => x.CreateModelExtraSwitch(It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Verifies if the Logic object calls the ReadBrand method correctly.
        /// </summary>
        [Test]
        public void BrandReadIsDone()
        {
            // Arrange
            var logic = new Logic();
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);

            // Act
            logic.Read("Brand");

            // Assert
            repoMock.Verify(x => x.ReadBrand(), Times.Once);
        }

        /// <summary>
        /// Verifies if the Logic object calls the ReadCar method correctly.
        /// </summary>
        [Test]
        public void CarReadIsDone()
        {
            // Arrange
            var logic = new Logic();
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);

            // Act
            logic.Read("Brand");

            // Assert
            repoMock.Verify(x => x.ReadBrand(), Times.Once);
        }

        /// <summary>
        /// Verifies if the Logic object calls the ReadExtra method correctly.
        /// </summary>
        [Test]
        public void ExtraReadIsDone()
        {
            // Arrange
            var logic = new Logic();
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);

            // Act
            logic.Read("Extra");

            // Assert
            repoMock.Verify(x => x.ReadExtra(), Times.Once);
        }

        /// <summary>
        /// Verifies if the Logic object calls the ReadModelExtraSwitch method correctly.
        /// </summary>
        [Test]
        public void ModelExtraSwitchReadIsDone()
        {
            // Arrange
            var logic = new Logic();
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);

            // Act
            logic.Read("ModelExtraSwitch");

            // Assert
            repoMock.Verify(x => x.ReadModelExtraSwitch(), Times.Once);
        }

        /// <summary>
        /// Verifies if the Logic object calls the UpdateBrand method correctly.
        /// </summary>
        [Test]
        public void BrandUpdateIsDone()
        {
            // Arrange
            var mock = new Mock<CarShopDatabaseEntities>();
            var logic = new Logic();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            Brand newbrand = new Brand()
            {
                ID = 9,
                Brandname = "Ford",
                Country = "Germany",
                url = "http://www.ford.com",
                Foundation_year = 1989,
                Yearly_income = 320000000,
            };
            Brand fresh = new Brand()
            {
                ID = 9,
                Brandname = "NotFord",
                Country = "NotGermany",
                url = "http://www.notford.com",
                Foundation_year = 1889,
                Yearly_income = 320000001,
            };
            mock.Setup(x => x.SaveChanges()).Verifiable();
            repoMock.Setup(x => x.ReadBrand()).Verifiable();

            // Act
            logic.Update("Brand", "9", "NotFord;NotGermany;http://www.notford.com;1889;320000001;");

            // Assert
            mock.Verify(x => x.SaveChanges(), Times.Once);
            Assert.That(newbrand.Brandname.Equals(fresh.Brandname));
            Assert.That(newbrand.Country.Equals(fresh.Country));
            Assert.That(newbrand.url.Equals(fresh.url));
            Assert.That(newbrand.Foundation_year.Equals(fresh.Foundation_year));
            Assert.That(newbrand.Yearly_income.Equals(fresh.Yearly_income));
        }

        /// <summary>
        /// Verifies if the Logic object calls the UpdateCar method correctly.
        /// </summary>
        [Test]
        public void CarUpdateIsDone()
        {
            // Arrange
            var mock = new Mock<CarShopDatabaseEntities>();
            var logic = new Logic();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            Car newcar = new Car()
            {
                ID = 12,
                name = "V12",
                release = 2010,
                engine_volume = 1200,
                horsepower = 1000,
                base_price = 3200000,
            };
            Car fresh = new Car()
            {
                ID = 12,
                name = "V13",
                release = 2013,
                engine_volume = 1203,
                horsepower = 1003,
                base_price = 3200003,
            };
            mock.Setup(x => x.SaveChanges()).Verifiable();
            repoMock.Setup(x => x.ReadCar()).Verifiable();

            // Act
            logic.Update("Car", "12", "V13;2013;1203;1003;3200003;");

            // Assert
            mock.Verify(x => x.SaveChanges(), Times.Once);
            Assert.That(newcar.name.Equals(fresh.name));
            Assert.That(newcar.release.Equals(fresh.release));
            Assert.That(newcar.engine_volume.Equals(fresh.engine_volume));
            Assert.That(newcar.horsepower.Equals(fresh.horsepower));
            Assert.That(newcar.base_price.Equals(fresh.base_price));
        }

        /// <summary>
        /// Verifies if the Logic object calls the UpdateExtra method correctly.
        /// </summary>
        [Test]
        public void ExtraUpdateIsDone()
        {
            // Arrange
            var mock = new Mock<CarShopDatabaseEntities>();
            var logic = new Logic();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            Extra newextra = new Extra()
            {
                ID = 14,
                category = "accessory",
                name = "spoiler",
                price = 5000,
                colour = "black",
                reusability = true,
            };
            Extra fresh = new Extra()
            {
                ID = 14,
                category = "notaccessory",
                name = "notspoiler",
                price = 5001,
                colour = "white",
                reusability = false,
            };
            mock.Setup(x => x.SaveChanges()).Verifiable();
            repoMock.Setup(x => x.ReadExtra()).Verifiable();

            // Act
            logic.Update("Extra", "14", "notaccessory;notspoiler;5001;white;false;");

            // Assert
            mock.Verify(x => x.SaveChanges(), Times.Once);
            Assert.That(newextra.category.Equals(fresh.category));
            Assert.That(newextra.name.Equals(fresh.name));
            Assert.That(newextra.price.Equals(fresh.price));
            Assert.That(newextra.colour.Equals(fresh.colour));
            Assert.That(newextra.reusability.Equals(fresh.reusability));
        }

        /// <summary>
        /// Verifies if the Logic object calls the UpdateModelExtraSwitch method correctly.
        /// </summary>
        [Test]
        public void ModelExtraSwitchUpdateIsDone()
        {
            // Arrange
            var mock = new Mock<CarShopDatabaseEntities>();
            var logic = new Logic();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            ModelExtraSwitch newmes = new ModelExtraSwitch()
            {
                ID = 18,
                car_id = 1,
                extra_id = 2,
            };
            ModelExtraSwitch fresh = new ModelExtraSwitch()
            {
                ID = 18,
                car_id = 2,
                extra_id = 3,
            };
            mock.Setup(x => x.SaveChanges()).Verifiable();
            repoMock.Setup(x => x.ReadModelExtraSwitch()).Verifiable();

            // Act
            logic.Update("ModelExtraSwitch", "18", "2;3;");

            // Assert
            mock.Verify(x => x.SaveChanges(), Times.Once);
            Assert.That(newmes.car_id.Equals(fresh.car_id));
            Assert.That(newmes.extra_id.Equals(fresh.extra_id));
        }

        /// <summary>
        /// Verifies if the Logic object calls the DeleteBrand method correctly.
        /// </summary>
        [Test]
        public void BrandDeleteIsDone()
        {
            // Arrange
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            var logic = new Logic();
            Brand brand = new Brand()
            {
                ID = 6,
                Brandname = "Brandname",
                Country = "Countryname",
                url = "http://www.randomurl.com",
                Foundation_year = 2077,
                Yearly_income = 9000,
            };
            repoMock.Setup(x => x.DeleteBrand(It.IsAny<string>())).Verifiable();
            repoMock.Setup(x => x.ReadBrand()).Verifiable();

            // Act
            logic.Delete("Brand", brand.ID.ToString());

            // Assert
            repoMock.Verify(x => x.DeleteBrand(It.IsAny<string>()), Times.Once);
            repoMock.Verify(x => x.ReadBrand(), Times.Once);
        }

        /// <summary>
        /// Verifies if the Logic object calls the DeleteCar method correctly.
        /// </summary>
        [Test]
        public void CarDeleteIsDone()
        {
            // Arrange
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            var logic = new Logic();
            Car car = new Car()
            {
                ID = 16,
                name = "Carname",
                release = 1761,
                engine_volume = 999,
                horsepower = 2,
                base_price = 5,
            };
            repoMock.Setup(x => x.DeleteCar(It.IsAny<string>())).Verifiable();
            repoMock.Setup(x => x.ReadCar()).Verifiable();

            // Act
            logic.Delete("Car", car.ID.ToString());

            // Assert
            repoMock.Verify(x => x.DeleteCar(It.IsAny<string>()), Times.Once);
            repoMock.Verify(x => x.ReadCar(), Times.Once);
        }

        /// <summary>
        /// Verifies if the Logic object calls the DeleteExtra method correctly.
        /// </summary>
        [Test]
        public void ExtraDeleteIsDone()
        {
            // Arrange
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            var logic = new Logic();
            Extra extra = new Extra()
            {
                ID = 89,
                category = "somecategory",
                name = "randomname",
                price = 90000,
                colour = "pink",
                reusability = false,
            };
            repoMock.Setup(x => x.DeleteExtra(It.IsAny<string>())).Verifiable();
            repoMock.Setup(x => x.ReadExtra()).Verifiable();

            // Act
            logic.Delete("Extra", extra.ID.ToString());

            // Assert
            repoMock.Verify(x => x.DeleteExtra(It.IsAny<string>()), Times.Once);
            repoMock.Verify(x => x.ReadExtra(), Times.Once);
        }

        /// <summary>
        /// Verifies if the Logic object calls the DeleteModelExtraSwitch method correctly.
        /// </summary>
        [Test]
        public void ModelExtraSwitchDeleteIsDone()
        {
            // Arrange
            var mock = new Mock<CarShopDatabaseEntities>();
            Mock<RepositoryC> repoMock = new Mock<RepositoryC>(mock.Object);
            var logic = new Logic();
            ModelExtraSwitch mes = new ModelExtraSwitch()
            {
                ID = 42,
                car_id = 8,
                extra_id = 9,
            };
            repoMock.Setup(x => x.DeleteModelExtraSwitch(It.IsAny<string>())).Verifiable();
            repoMock.Setup(x => x.ReadModelExtraSwitch()).Verifiable();

            // Act
            logic.Delete("ModelExtraSwitch", mes.ID.ToString());

            // Assert
            repoMock.Verify(x => x.DeleteModelExtraSwitch(It.IsAny<string>()), Times.Once);
            repoMock.Verify(x => x.ReadModelExtraSwitch(), Times.Once);
        }

        /// <summary>
        /// Verifies if Logic object calls the CountCategories method correctly.
        /// </summary>
        [Test]
        public void CountCategoriesIsWorking()
        {
            // Arrange
            var mock = new Mock<CarShopDatabaseEntities>();
            var logic = new Logic();
            var repoMock = new Mock<RepositoryC>(mock.Object);

            repoMock.Setup(x => x.CountCategories()).Verifiable();

            // Act
            logic.AvaragePrice();

            // Arrange
            repoMock.Verify(x => x.CountCategories(), Times.Once);
        }

        /// <summary>
        /// Verifies if Logic object calls the GetFullPrice method correctly.
        /// </summary>
        public void GetFullPriceIsWorking()
        {
            // Arrange
            var mock = new Mock<CarShopDatabaseEntities>();
            var logic = new Logic();
            var repoMock = new Mock<RepositoryC>(mock.Object);
            Car car = new Car()
            {
                ID = 36,
                name = "V2",
                release = 2010,
                engine_volume = 1200,
                horsepower = 1000,
                base_price = 3200000,
            };
            Extra extra = new Extra()
            {
                ID = 27,
                category = "accessory",
                name = "spoiler",
                price = 7000,
                colour = "white",
                reusability = false,
            };
            ModelExtraSwitch mes = new ModelExtraSwitch();
            mes.ID = 22;
            mes.car_id = car.ID;
            mes.extra_id = extra.ID;
            repoMock.Setup(x => x.GetFullPrice(It.IsAny<string>())).Verifiable();

            // Act
            decimal final = logic.GetFullPrice("36");
            decimal fullprice = (decimal)car.base_price + (decimal)extra.price;

            // Assert
            Assert.That(fullprice.Equals(final));
        }

        /// <summary>
        /// Verifies if the Logic object calls the AvaragePrice method correctly.
        /// </summary>
        [Test]
        public void AvaragePriceIsWorking()
        {
            // Arrange
            var mock = new Mock<CarShopDatabaseEntities>();
            var logic = new Logic();
            var repoMock = new Mock<RepositoryC>(mock.Object);

            repoMock.Setup(x => x.AvaragePrice()).Verifiable();

            // Act
            logic.AvaragePrice();

            // Arrange
            repoMock.Verify(x => x.AvaragePrice(), Times.Once);
        }
    }
}
