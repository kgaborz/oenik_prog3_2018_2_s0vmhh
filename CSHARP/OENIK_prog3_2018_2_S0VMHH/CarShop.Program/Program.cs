﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.ILogic;

    /// <summary>
    /// Program class.
    /// Executes the main methods and operations at once.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Main method, which creates the menu.
        /// </summary>
        internal static void Main()
        {
            var logic = new Logic();
            var menu = new Menu();
            menu.Open(logic);
        }
    }
}
