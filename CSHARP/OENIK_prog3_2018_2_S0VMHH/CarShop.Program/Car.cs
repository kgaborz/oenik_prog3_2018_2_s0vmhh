﻿// <copyright file="Car.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using CarShop.ILogic;

    /// <summary>
    /// Car class, which handles the <see cref="Menu"/>'s 'Car' option.
    /// </summary>
    internal class Car
    {
        /// <summary>
        /// GetCarMethods gets all of the operations that you can do with the Car table.
        /// </summary>
        /// <param name="logic">A <see cref="ILogicI"/> object which can handle the table's operations.</param>
        public void GetCarMethods(ILogicI logic)
        {
            Console.Clear();
            Console.WriteLine("1: Create a Car in Car table.\n2: Get a Car from the Car table.\n3: Update a Car's details.\n4: Remove a Car from Car table.\n5: Get a car's full price (Base price + extras)\n6: Get a bid on a car.");
            var k = Console.ReadKey();
            string t = "Car";
            switch (k.Key)
            {
                case ConsoleKey.D1:
                    Console.Clear();
                    Console.WriteLine("Please enter an input for the new Car!");
                    string input = Console.ReadLine();
                    logic.Create(t, input);
                    break;
                case ConsoleKey.D2:
                    Console.Clear();
                    logic.Read(t);
                    break;
                case ConsoleKey.D3:
                    Console.Clear();
                    Console.WriteLine("Please enter the Car's ID that you want to update!");
                    string id = Console.ReadLine();
                    Console.WriteLine("Now enter the new data.");
                    string newdata = Console.ReadLine();
                    logic.Update(t, id, newdata);
                    break;
                case ConsoleKey.D4:
                    Console.Clear();
                    Console.WriteLine("Please enter the Car's ID that you want to delete!");
                    string remove = Console.ReadLine();
                    logic.Delete(t, remove);
                    break;
                case ConsoleKey.D5:
                    Console.Clear();
                    Console.WriteLine("Please enter the Car's ID!");
                    string pid = Console.ReadLine();
                    logic.GetFullPrice(pid);
                    break;
                case ConsoleKey.D6:
                    Console.Clear();
                    Console.WriteLine("Enter a car name.");
                    var s = Console.ReadLine();
                    Console.WriteLine("Enter a base price.");
                    var p = Console.ReadLine();
                    logic.WebInteraction(s, p);
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Thats not a valid option.");
                    break;
            }

            Console.ReadKey();
        }
    }
}
