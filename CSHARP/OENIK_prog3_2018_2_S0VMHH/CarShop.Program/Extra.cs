﻿// <copyright file="Extra.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.ILogic;

    /// <summary>
    /// Extra class, which handles the <see cref="Menu"/>'s 'Extra' option.
    /// </summary>
    internal class Extra
    {
        /// <summary>
        /// GetExtraMethods gets all of the operations that you can do with the Extra table.
        /// </summary>
        /// <param name="logic"> A <see cref="ILogicI"/> object which can handle the table's operations.</param>
        public void GetExtraMethods(ILogicI logic)
        {
            Console.Clear();
            Console.WriteLine("1: Create an Extra in Extra table.\n2: Get an Extra from the Extra table.\n3: Update an Extra's details.\n4: Remove an Extra from Extra table.\n5:Count how many extras are connected to a car from each type of categories.");
            var k = Console.ReadKey();
            string t = "Extra";
            switch (k.Key)
            {
                case ConsoleKey.D1:
                    Console.Clear();
                    Console.WriteLine("Please enter an input for the new Extra!");
                    string input = Console.ReadLine();
                    logic.Create(t, input);
                    break;
                case ConsoleKey.D2:
                    Console.Clear();
                    logic.Read(t);
                    break;
                case ConsoleKey.D3:
                    Console.Clear();
                    Console.WriteLine("Please enter the Extra's ID that you want to update!");
                    string id = Console.ReadLine();
                    Console.WriteLine("Now enter the new data.");
                    string newdata = Console.ReadLine();
                    logic.Update(t, id, newdata);
                    break;
                case ConsoleKey.D4:
                    Console.Clear();
                    Console.WriteLine("Please enter the Extra's ID that you want to delete!");
                    string remove = Console.ReadLine();
                    logic.Delete(t, remove);
                    break;
                case ConsoleKey.D5:
                    Console.Clear();
                    logic.CountCategories();
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Thats not a valid option.");
                    break;
            }

            Console.ReadKey();
        }
    }
}
