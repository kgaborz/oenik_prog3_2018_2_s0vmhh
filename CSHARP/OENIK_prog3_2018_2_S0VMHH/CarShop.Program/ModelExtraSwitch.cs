﻿// <copyright file="ModelExtraSwitch.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.ILogic;

    /// <summary>
    /// ModelExtraSwitch class, which handles the <see cref="Menu"/>'s 'ModelExtraSwitch' option.
    /// </summary>
    internal class ModelExtraSwitch
    {
        /// <summary>
        /// GetModelExtraSwitchMethods gets all of the operations that you can do with the ModelExtraSwitch table.
        /// </summary>
        /// <param name="logic">A <see cref="ILogicI"/> object which can handle the table's operations.</param>
        public void GetModelExtraSwitchMethods(ILogicI logic)
        {
            Console.Clear();
            Console.WriteLine("1: Create a Switch in ModelExtraSwitch table.\n2: Get the switches from the ModelExtraSwtich table.\n3: Update a Switch in ModelExtraSwitch table. \n4: Remove a Switch from ModelExtraSwitch table.");
            var k = Console.ReadKey();
            string t = "ModelExtraSwitch";
            switch (k.Key)
            {
                case ConsoleKey.D1:
                    Console.Clear();
                    Console.WriteLine("Please enter an input for the new ModelExtraSwitch!");
                    string input = Console.ReadLine();
                    logic.Create(t, input);
                    break;
                case ConsoleKey.D2:
                    Console.Clear();
                    logic.Read(t);
                    break;
                case ConsoleKey.D3:
                    Console.Clear();
                    Console.WriteLine("Please enter the ModelExtraSwitch's ID that you want to update!");
                    string id = Console.ReadLine();
                    Console.WriteLine("Now enter the new data.");
                    string newdata = Console.ReadLine();
                    logic.Update(t, id, newdata);
                    break;
                case ConsoleKey.D4:
                    Console.Clear();
                    Console.WriteLine("Please enter the ModelExtraSwitch's ID that you want to delete!");
                    string remove = Console.ReadLine();
                    logic.Delete(t, remove);
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Thats not a valid option.");
                    break;
            }

            Console.ReadKey();
        }
    }
}
