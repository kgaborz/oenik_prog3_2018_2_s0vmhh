﻿// <copyright file="Brand.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.ILogic;

    /// <summary>
    /// Brand class, which handles the <see cref="Menu"/>'s 'Brand' option.
    /// </summary>
    internal class Brand
    {
        /// <summary>
        /// GetBrandMethods gets all of the operations that you can do with the Brand table.
        /// </summary>
        /// <param name="logic">A <see cref="ILogicI"/> object which can handle the table's operations.</param>
        public void GetBrandMethods(ILogicI logic)
        {
            Console.Clear();
            Console.WriteLine("1: Create a Brand in Brand table.\n2: Get a Brand from the Brand table.\n3: Update a Brand's details.\n4: Remove a Brand from Brand table.\n5: Get the avarage price all of the cars from each brands.");
            var k = Console.ReadKey();
            string t = "Brand";
            switch (k.Key)
            {
                case ConsoleKey.D1:
                    Console.Clear();
                    Console.WriteLine("Please enter an input for the new Brand!");
                    string input = Console.ReadLine();
                    logic.Create(t, input);
                    break;
                case ConsoleKey.D2:
                    Console.Clear();
                    logic.Read(t);
                    break;
                case ConsoleKey.D3:
                    Console.Clear();
                    Console.WriteLine("Please enter the Brand's ID that you want to update!");
                    string id = Console.ReadLine();
                    Console.WriteLine("Now enter the new data.");
                    string newdata = Console.ReadLine();
                    logic.Update(t, id, newdata);
                    break;
                case ConsoleKey.D4:
                    Console.Clear();
                    Console.WriteLine("Please enter the ModelExtraSwitch's ID that you want to delete!");
                    string remove = Console.ReadLine();
                    logic.Delete(t, remove);
                    break;
                case ConsoleKey.D5:
                    Console.Clear();
                    logic.AvaragePrice();
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Thats not a valid option.");
                    break;
            }

            Console.ReadKey();
        }
    }
}
