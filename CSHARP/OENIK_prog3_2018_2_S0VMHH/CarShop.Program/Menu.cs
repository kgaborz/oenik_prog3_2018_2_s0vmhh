﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using CarShop.ILogic;

    /// <summary>
    /// Menu class, which builds up the Menu.
    /// </summary>
    internal class Menu
    {
        /// <summary>
        /// Open method.
        /// It handles the <see cref="Menu"/> class and handles the menu.
        /// </summary>
        /// <param name="logic">A <see cref="ILogicI"/> object which can handle the tables' operations.</param>
        internal void Open(ILogicI logic)
        {
            Console.WriteLine("Choose from the following options:\n 1: Manage Car table.\n 2: Manage Brand table.\n 3: Manage Extra table.\n 4: Manage ModelExtraSwitch table.");
            var k = Console.ReadKey();
            switch (k.Key)
            {
                case ConsoleKey.D1:
                    Car c = new Car();
                    c.GetCarMethods(logic);
                    break;
                case ConsoleKey.D2:
                    Brand b = new Brand();
                    b.GetBrandMethods(logic);
                    break;
                case ConsoleKey.D3:
                    Extra e = new Extra();
                    e.GetExtraMethods(logic);
                    break;
                case ConsoleKey.D4:
                    ModelExtraSwitch mes = new ModelExtraSwitch();
                    mes.GetModelExtraSwitchMethods(logic);
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Thats not a valid option.");
                    break;
            }

            Console.ReadKey();
        }
    }
}