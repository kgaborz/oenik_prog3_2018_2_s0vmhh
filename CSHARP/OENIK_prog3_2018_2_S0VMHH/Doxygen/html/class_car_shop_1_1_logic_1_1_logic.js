var class_car_shop_1_1_logic_1_1_logic =
[
    [ "Logic", "class_car_shop_1_1_logic_1_1_logic.html#af006d6f092e7d6adabc05cb8e546d829", null ],
    [ "AvaragePrice", "class_car_shop_1_1_logic_1_1_logic.html#a44319204225a574ac08569289839bd92", null ],
    [ "CountCategories", "class_car_shop_1_1_logic_1_1_logic.html#a27e2cfea7b54ae50a1464b1dba4fb82d", null ],
    [ "Create", "class_car_shop_1_1_logic_1_1_logic.html#acf9d5d63313bd5698835278a557610ee", null ],
    [ "Delete", "class_car_shop_1_1_logic_1_1_logic.html#a51ea53d09f7228b24bcf448ff2278c51", null ],
    [ "GetFullPrice", "class_car_shop_1_1_logic_1_1_logic.html#aaa5dc023eb1735cabd315f5eb3611aaf", null ],
    [ "Read", "class_car_shop_1_1_logic_1_1_logic.html#aceb2098f8d4b692ed99bbfa9c0684207", null ],
    [ "Update", "class_car_shop_1_1_logic_1_1_logic.html#ab81f7ad1398d482685c1da604dd76c31", null ],
    [ "WebInteraction", "class_car_shop_1_1_logic_1_1_logic.html#ac5ee10d3f9e325052a359317dfedd2e3", null ]
];