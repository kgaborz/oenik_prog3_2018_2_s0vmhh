var dir_08f56e0c3c1af3b218112088555e354f =
[
    [ "CarShop.Data", "dir_5ba5879ac43a14401dfc2034e4b84e29.html", "dir_5ba5879ac43a14401dfc2034e4b84e29" ],
    [ "CarShop.JavaWeb", "dir_72a1bb19eb586fede2fdf1f9878829aa.html", "dir_72a1bb19eb586fede2fdf1f9878829aa" ],
    [ "CarShop.Logic", "dir_6dbae81bd60127092673fa276c2c2f44.html", "dir_6dbae81bd60127092673fa276c2c2f44" ],
    [ "CarShop.Logic.Tests", "dir_8eb481aa6e220fcd46080e2f3adc45eb.html", "dir_8eb481aa6e220fcd46080e2f3adc45eb" ],
    [ "CarShop.Program", "dir_40892b98e3f278cf04d59d08b7e9ce69.html", "dir_40892b98e3f278cf04d59d08b7e9ce69" ],
    [ "CarShop.Repository", "dir_d6e331fdec7a78dd0c132021013b4fd4.html", "dir_d6e331fdec7a78dd0c132021013b4fd4" ],
    [ "CarShop.Repository.Tests", "dir_4458a8d352e5681cd3c7b81697b77b7f.html", "dir_4458a8d352e5681cd3c7b81697b77b7f" ]
];