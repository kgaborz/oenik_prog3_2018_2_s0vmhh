var interface_car_shop_1_1_logic_1_1_i_logic =
[
    [ "AvaragePrice", "interface_car_shop_1_1_logic_1_1_i_logic.html#a9ab094b77195989954289e84214e442b", null ],
    [ "CountCategories", "interface_car_shop_1_1_logic_1_1_i_logic.html#a5e700b0d3e544c006a30eb33eebfeff0", null ],
    [ "Create", "interface_car_shop_1_1_logic_1_1_i_logic.html#ab9b4e33d7fd3ca93858e1568347d2bf2", null ],
    [ "Delete", "interface_car_shop_1_1_logic_1_1_i_logic.html#a51f3324a821af930e30859258db7a14a", null ],
    [ "GetFullPrice", "interface_car_shop_1_1_logic_1_1_i_logic.html#ad9c3004997713eb619552a6f643891a0", null ],
    [ "Read", "interface_car_shop_1_1_logic_1_1_i_logic.html#aebcbce4ecadbe09d153f263e1913a100", null ],
    [ "Update", "interface_car_shop_1_1_logic_1_1_i_logic.html#a12f9f1557ae7eb9c555df6f34d1639b4", null ],
    [ "WebInteraction", "interface_car_shop_1_1_logic_1_1_i_logic.html#a074e8d57c3c12e3289704ebcfde398fc", null ]
];