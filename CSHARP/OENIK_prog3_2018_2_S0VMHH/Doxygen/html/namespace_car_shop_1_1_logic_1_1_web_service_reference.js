var namespace_car_shop_1_1_logic_1_1_web_service_reference =
[
    [ "FileNotFoundException", "class_car_shop_1_1_logic_1_1_web_service_reference_1_1_file_not_found_exception.html", "class_car_shop_1_1_logic_1_1_web_service_reference_1_1_file_not_found_exception" ],
    [ "GetPriceRequest", "class_car_shop_1_1_logic_1_1_web_service_reference_1_1_get_price_request.html", "class_car_shop_1_1_logic_1_1_web_service_reference_1_1_get_price_request" ],
    [ "GetPriceResponse", "class_car_shop_1_1_logic_1_1_web_service_reference_1_1_get_price_response.html", "class_car_shop_1_1_logic_1_1_web_service_reference_1_1_get_price_response" ],
    [ "ParserConfigurationException", "class_car_shop_1_1_logic_1_1_web_service_reference_1_1_parser_configuration_exception.html", "class_car_shop_1_1_logic_1_1_web_service_reference_1_1_parser_configuration_exception" ],
    [ "WebClient", "interface_car_shop_1_1_logic_1_1_web_service_reference_1_1_web_client.html", "interface_car_shop_1_1_logic_1_1_web_service_reference_1_1_web_client" ],
    [ "WebClientChannel", "interface_car_shop_1_1_logic_1_1_web_service_reference_1_1_web_client_channel.html", null ],
    [ "WebClientClient", "class_car_shop_1_1_logic_1_1_web_service_reference_1_1_web_client_client.html", "class_car_shop_1_1_logic_1_1_web_service_reference_1_1_web_client_client" ]
];