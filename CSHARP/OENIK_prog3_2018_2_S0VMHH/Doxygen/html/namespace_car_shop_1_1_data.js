var namespace_car_shop_1_1_data =
[
    [ "Brand", "class_car_shop_1_1_data_1_1_brand.html", "class_car_shop_1_1_data_1_1_brand" ],
    [ "Car", "class_car_shop_1_1_data_1_1_car.html", "class_car_shop_1_1_data_1_1_car" ],
    [ "CarShopDatabaseEntities", "class_car_shop_1_1_data_1_1_car_shop_database_entities.html", "class_car_shop_1_1_data_1_1_car_shop_database_entities" ],
    [ "Extra", "class_car_shop_1_1_data_1_1_extra.html", "class_car_shop_1_1_data_1_1_extra" ],
    [ "ModelExtraSwitch", "class_car_shop_1_1_data_1_1_model_extra_switch.html", "class_car_shop_1_1_data_1_1_model_extra_switch" ]
];