var hierarchy =
[
    [ "CarShop.Data.Brand", "class_car_shop_1_1_data_1_1_brand.html", null ],
    [ "CarShop.Data.Car", "class_car_shop_1_1_data_1_1_car.html", null ],
    [ "ClientBase", null, [
      [ "CarShop.JavaWeb.WebServiceReference.WebClientClient", "class_car_shop_1_1_java_web_1_1_web_service_reference_1_1_web_client_client.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "CarShop.Data.CarShopDatabaseEntities", "class_car_shop_1_1_data_1_1_car_shop_database_entities.html", null ]
    ] ],
    [ "CarShop.Data.Extra", "class_car_shop_1_1_data_1_1_extra.html", null ],
    [ "CarShop.JavaWeb.WebServiceReference.GetPriceRequest", "class_car_shop_1_1_java_web_1_1_web_service_reference_1_1_get_price_request.html", null ],
    [ "CarShop.JavaWeb.WebServiceReference.GetPriceResponse", "class_car_shop_1_1_java_web_1_1_web_service_reference_1_1_get_price_response.html", null ],
    [ "IClientChannel", null, [
      [ "CarShop.JavaWeb.WebServiceReference.WebClientChannel", "interface_car_shop_1_1_java_web_1_1_web_service_reference_1_1_web_client_channel.html", null ]
    ] ],
    [ "IHttpHandler", null, [
      [ "CarShop.JavaWeb.Console", "class_car_shop_1_1_java_web_1_1_console.html", null ]
    ] ],
    [ "CarShop.ILogic.ILogicI", "interface_car_shop_1_1_i_logic_1_1_i_logic_i.html", [
      [ "CarShop.ILogic.Logic", "class_car_shop_1_1_i_logic_1_1_logic.html", null ]
    ] ],
    [ "INotifyPropertyChanged", null, [
      [ "CarShop.JavaWeb.WebServiceReference.FileNotFoundException", "class_car_shop_1_1_java_web_1_1_web_service_reference_1_1_file_not_found_exception.html", null ]
    ] ],
    [ "CarShop.ILogic.Tests.LogicTest", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html", null ],
    [ "CarShop.Data.ModelExtraSwitch", "class_car_shop_1_1_data_1_1_model_extra_switch.html", null ],
    [ "object", null, [
      [ "CarShop.JavaWeb.WebServiceReference.FileNotFoundException", "class_car_shop_1_1_java_web_1_1_web_service_reference_1_1_file_not_found_exception.html", null ]
    ] ],
    [ "CarShop.JavaWeb.Program", "class_car_shop_1_1_java_web_1_1_program.html", null ],
    [ "CarShop.Repository.RepositoryC", "class_car_shop_1_1_repository_1_1_repository_c.html", null ],
    [ "CarShop.RepositoryC.Tests.RepoTests", "class_car_shop_1_1_repository_c_1_1_tests_1_1_repo_tests.html", null ],
    [ "CarShop.JavaWeb.WebServiceReference.WebClient", "interface_car_shop_1_1_java_web_1_1_web_service_reference_1_1_web_client.html", [
      [ "CarShop.JavaWeb.WebServiceReference.WebClientChannel", "interface_car_shop_1_1_java_web_1_1_web_service_reference_1_1_web_client_channel.html", null ],
      [ "CarShop.JavaWeb.WebServiceReference.WebClientClient", "class_car_shop_1_1_java_web_1_1_web_service_reference_1_1_web_client_client.html", null ]
    ] ]
];