var searchData=
[
  ['read',['Read',['../interface_car_shop_1_1_logic_1_1_i_logic.html#aebcbce4ecadbe09d153f263e1913a100',1,'CarShop.Logic.ILogic.Read()'],['../class_car_shop_1_1_logic_1_1_logic.html#aceb2098f8d4b692ed99bbfa9c0684207',1,'CarShop.Logic.Logic.Read()']]],
  ['readbrand',['ReadBrand',['../class_car_shop_1_1_repository_1_1_repository.html#a798e078ad7c9cd827d642834e23cc332',1,'CarShop::Repository::Repository']]],
  ['readcar',['ReadCar',['../class_car_shop_1_1_repository_1_1_repository.html#af97bb9cb50ae16e25042072eba542488',1,'CarShop::Repository::Repository']]],
  ['readextra',['ReadExtra',['../class_car_shop_1_1_repository_1_1_repository.html#ad03078d548edc70359587f61508a2ecf',1,'CarShop::Repository::Repository']]],
  ['readmodelextraswitch',['ReadModelExtraSwitch',['../class_car_shop_1_1_repository_1_1_repository.html#aa037b95d81a9e27ed0ec4a498b1d8335',1,'CarShop::Repository::Repository']]],
  ['release',['release',['../class_car_shop_1_1_data_1_1_car.html#a0128536c314fd4af3c820dfd4814cb84',1,'CarShop::Data::Car']]],
  ['repository',['Repository',['../class_car_shop_1_1_repository_1_1_repository.html',1,'CarShop::Repository']]],
  ['repository_2ecs',['Repository.cs',['../_repository_8cs.html',1,'']]],
  ['reusability',['reusability',['../class_car_shop_1_1_data_1_1_extra.html#a0162b3f4c630a0b49a101b2befea64d6',1,'CarShop::Data::Extra']]]
];
