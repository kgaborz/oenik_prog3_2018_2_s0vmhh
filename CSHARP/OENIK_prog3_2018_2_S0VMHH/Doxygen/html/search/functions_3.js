var searchData=
[
  ['delete',['Delete',['../interface_car_shop_1_1_i_logic_1_1_i_logic_i.html#a27d3ebc88dc90adb6e1db4eec52c95ad',1,'CarShop.ILogic.ILogicI.Delete()'],['../class_car_shop_1_1_i_logic_1_1_logic.html#a4b0f0b2072534f0bc36962fa98883dd2',1,'CarShop.ILogic.Logic.Delete()']]],
  ['deletebrand',['DeleteBrand',['../class_car_shop_1_1_repository_1_1_repository_c.html#a64b615b66f81a40fd6a973d555bbd9f7',1,'CarShop::Repository::RepositoryC']]],
  ['deletecar',['DeleteCar',['../class_car_shop_1_1_repository_1_1_repository_c.html#ac784091e773f2a3ba04e7f9a9a6e4a7e',1,'CarShop::Repository::RepositoryC']]],
  ['deleteextra',['DeleteExtra',['../class_car_shop_1_1_repository_1_1_repository_c.html#a4791098699d42542e22aa331cc40b9d5',1,'CarShop::Repository::RepositoryC']]],
  ['deletemodelextraswitch',['DeleteModelExtraSwitch',['../class_car_shop_1_1_repository_1_1_repository_c.html#a47aa58da75eb61774a18a5e7ee8fa055',1,'CarShop::Repository::RepositoryC']]]
];
