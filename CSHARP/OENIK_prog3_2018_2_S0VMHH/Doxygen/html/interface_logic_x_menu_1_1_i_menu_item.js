var interface_logic_x_menu_1_1_i_menu_item =
[
    [ "Command", "interface_logic_x_menu_1_1_i_menu_item.html#af02c3f5b37f50dd4e1973bdd7f1c6c47", null ],
    [ "Description", "interface_logic_x_menu_1_1_i_menu_item.html#a10f7cb831b7a80d69bc058a0760897c3", null ],
    [ "HasCommand", "interface_logic_x_menu_1_1_i_menu_item.html#a64b964aabf5ecdc3de33f225df6d3735", null ],
    [ "HasMenuItems", "interface_logic_x_menu_1_1_i_menu_item.html#aebaccd8bf9f80136118dd2c814dba515", null ],
    [ "KeyInfo", "interface_logic_x_menu_1_1_i_menu_item.html#aff878f8f0c932718ce05a9b68e8423dc", null ],
    [ "MenuItems", "interface_logic_x_menu_1_1_i_menu_item.html#ad2ba06ce0415f14a547fd6accf5253a3", null ],
    [ "Name", "interface_logic_x_menu_1_1_i_menu_item.html#a1267764d70916434fa8ba632be42a6dc", null ],
    [ "Parent", "interface_logic_x_menu_1_1_i_menu_item.html#af95f871e326a4f8dbd94d99b9dbac5b4", null ]
];