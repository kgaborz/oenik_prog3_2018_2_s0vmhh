var class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test =
[
    [ "AvaragePriceIsWorking", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#aba0179c3aa232f2bf0cc75abc968e314", null ],
    [ "BrandCreationIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a8b88eb3e759e2c14ee5953e048c8cd00", null ],
    [ "BrandDeleteIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a40e65e5673ae9a86048472803f05b456", null ],
    [ "BrandReadIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a51314109407a468c9c8abce5e7aacc3a", null ],
    [ "BrandUpdateIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#ab491b185062e51837f93b5ee7470b64e", null ],
    [ "CarCreationIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#ab655d20c3b6367dbb0ae152fdeede22a", null ],
    [ "CarDeleteIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a5ba3ef3863c4e0209e6315498db9079c", null ],
    [ "CarReadIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a12148ac14e4500d69b1bf69403435c5d", null ],
    [ "CarUpdateIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a5daa2ab6e6c2f26dddf2f83a4d724120", null ],
    [ "CountCategoriesIsWorking", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a2047e31b433f9f67d25538f34f81c835", null ],
    [ "ExtraCreationIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#aaba0f1744b0bc4e206c356e4ab6e16c2", null ],
    [ "ExtraDeleteIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a487abdd4be4cfae5c57f0f04677c3e50", null ],
    [ "ExtraReadIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#aabe789edd2bcd4da77d86ff3233c6600", null ],
    [ "ExtraUpdateIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a2859c9adfb1b76c67086a36a5d020f41", null ],
    [ "GetFullPriceIsWorking", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a23bb81b51465db0e007efd226c7687f2", null ],
    [ "ModelExtraSwitchCreationIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#aecd3f8d64591e4900ba99de33c805a71", null ],
    [ "ModelExtraSwitchDeleteIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#ac19546614944e5873cf39eaeee84c260", null ],
    [ "ModelExtraSwitchReadIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a44de2312c07090d7e0744706f7fc4fb1", null ],
    [ "ModelExtraSwitchUpdateIsDone", "class_car_shop_1_1_i_logic_1_1_tests_1_1_logic_test.html#a5dccceb4a1cb69e6fc8e2870329e0151", null ]
];