var dir_5ba5879ac43a14401dfc2034e4b84e29 =
[
    [ "obj", "dir_563e1f40dd573252687167d6ee372556.html", "dir_563e1f40dd573252687167d6ee372556" ],
    [ "Properties", "dir_5d0c00b83b8bd5baaaacb579eb6e7b6f.html", "dir_5d0c00b83b8bd5baaaacb579eb6e7b6f" ],
    [ "Brand.cs", "_car_shop_8_data_2_brand_8cs.html", [
      [ "Brand", "class_car_shop_1_1_data_1_1_brand.html", "class_car_shop_1_1_data_1_1_brand" ]
    ] ],
    [ "Car.cs", "_car_shop_8_data_2_car_8cs.html", [
      [ "Car", "class_car_shop_1_1_data_1_1_car.html", "class_car_shop_1_1_data_1_1_car" ]
    ] ],
    [ "CarSHopModel.Context.cs", "_car_s_hop_model_8_context_8cs.html", [
      [ "CarShopDatabaseEntities", "class_car_shop_1_1_data_1_1_car_shop_database_entities.html", "class_car_shop_1_1_data_1_1_car_shop_database_entities" ]
    ] ],
    [ "CarSHopModel.cs", "_car_s_hop_model_8cs.html", null ],
    [ "CarSHopModel.Designer.cs", "_car_s_hop_model_8_designer_8cs.html", null ],
    [ "Extra.cs", "_car_shop_8_data_2_extra_8cs.html", [
      [ "Extra", "class_car_shop_1_1_data_1_1_extra.html", "class_car_shop_1_1_data_1_1_extra" ]
    ] ],
    [ "ModelExtraSwitch.cs", "_car_shop_8_data_2_model_extra_switch_8cs.html", [
      [ "ModelExtraSwitch", "class_car_shop_1_1_data_1_1_model_extra_switch.html", "class_car_shop_1_1_data_1_1_model_extra_switch" ]
    ] ]
];