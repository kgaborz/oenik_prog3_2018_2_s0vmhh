var class_car_shop_1_1_data_1_1_car =
[
    [ "Car", "class_car_shop_1_1_data_1_1_car.html#a7b0250407f416b9c97a9bad48542d0f8", null ],
    [ "base_price", "class_car_shop_1_1_data_1_1_car.html#ae71a9c06e152c256f9aae70efdc3b3d0", null ],
    [ "Brand", "class_car_shop_1_1_data_1_1_car.html#a413213242e3fb201efe66a6f642ba91f", null ],
    [ "brand_id", "class_car_shop_1_1_data_1_1_car.html#a41e04f93c0500d8d69d6464cd72e68a4", null ],
    [ "engine_volume", "class_car_shop_1_1_data_1_1_car.html#a1756c25f4595dec87f9292056ee00cdb", null ],
    [ "horsepower", "class_car_shop_1_1_data_1_1_car.html#a42f392d983f53572f49bfe60792411f7", null ],
    [ "ID", "class_car_shop_1_1_data_1_1_car.html#a9b1a4bed09d9c627f004cb401d64e3da", null ],
    [ "ModelExtraSwitches", "class_car_shop_1_1_data_1_1_car.html#a7d9c529a27c1c28279bfd58578ba0760", null ],
    [ "name", "class_car_shop_1_1_data_1_1_car.html#ab9d7389b610512e5164a1fd5353a4916", null ],
    [ "release", "class_car_shop_1_1_data_1_1_car.html#a0128536c314fd4af3c820dfd4814cb84", null ]
];