var namespace_car_shop_1_1_logic =
[
    [ "Tests", "namespace_car_shop_1_1_logic_1_1_tests.html", "namespace_car_shop_1_1_logic_1_1_tests" ],
    [ "WebServiceReference", "namespace_car_shop_1_1_logic_1_1_web_service_reference.html", "namespace_car_shop_1_1_logic_1_1_web_service_reference" ],
    [ "ILogic", "interface_car_shop_1_1_logic_1_1_i_logic.html", "interface_car_shop_1_1_logic_1_1_i_logic" ],
    [ "Logic", "class_car_shop_1_1_logic_1_1_logic.html", "class_car_shop_1_1_logic_1_1_logic" ]
];