var class_car_shop_1_1_data_1_1_car_shop_database_entities =
[
    [ "CarShopDatabaseEntities", "class_car_shop_1_1_data_1_1_car_shop_database_entities.html#abd736f0576bed88b8d7a2c3cb400ce8f", null ],
    [ "OnModelCreating", "class_car_shop_1_1_data_1_1_car_shop_database_entities.html#af8b0edbf7ab1c3949d6f726622a62480", null ],
    [ "Brand", "class_car_shop_1_1_data_1_1_car_shop_database_entities.html#af4f5aa5e60be34792575d3d9c2ea028f", null ],
    [ "Car", "class_car_shop_1_1_data_1_1_car_shop_database_entities.html#a518fa8fc6054b98cac2c08beec5f45bb", null ],
    [ "Extra", "class_car_shop_1_1_data_1_1_car_shop_database_entities.html#aaaf6fa0b22dfa45fc1f39b59a00a672b", null ],
    [ "ModelExtraSwitch", "class_car_shop_1_1_data_1_1_car_shop_database_entities.html#a29446b949662bca1ee41d3de56afaa96", null ]
];