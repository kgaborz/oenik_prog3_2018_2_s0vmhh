var interface_car_shop_1_1_repository_1_1_i_repository =
[
    [ "CreateBrand", "interface_car_shop_1_1_repository_1_1_i_repository.html#a5e9ec07d84441cd606144d7f00ceb570", null ],
    [ "CreateCar", "interface_car_shop_1_1_repository_1_1_i_repository.html#a6bf9a58d3652993d0366f25b7ad807a8", null ],
    [ "CreateExtra", "interface_car_shop_1_1_repository_1_1_i_repository.html#a9f7d82fbf8d169fdc150ff0c77d62b9a", null ],
    [ "CreateModelExtraSwitch", "interface_car_shop_1_1_repository_1_1_i_repository.html#adec147c0d017adf6969c04598cf61f74", null ],
    [ "DeleteBrand", "interface_car_shop_1_1_repository_1_1_i_repository.html#ac585db927af0783bb1593b459ae31ca7", null ],
    [ "DeleteCar", "interface_car_shop_1_1_repository_1_1_i_repository.html#aa97b3cd1d885371e54edd9ec7dae168d", null ],
    [ "DeleteExtra", "interface_car_shop_1_1_repository_1_1_i_repository.html#adf9641c45604f991d6cba07bcabde028", null ],
    [ "DeleteModelExtraSwitch", "interface_car_shop_1_1_repository_1_1_i_repository.html#ae3204321f9488aa1e96fbb98a20fd28e", null ],
    [ "ReadBrand", "interface_car_shop_1_1_repository_1_1_i_repository.html#addb0dc530fa8fcc617123691d4427ebe", null ],
    [ "ReadCar", "interface_car_shop_1_1_repository_1_1_i_repository.html#a262bbe684d0f7a8bc4cfec73d4b35d53", null ],
    [ "ReadExtra", "interface_car_shop_1_1_repository_1_1_i_repository.html#ab74203ed7e239858f62e4a38fb08e014", null ],
    [ "ReadModelExtraSwitch", "interface_car_shop_1_1_repository_1_1_i_repository.html#affa85f3c7656229de33d5d2cd53f2aef", null ],
    [ "UpdateBrand", "interface_car_shop_1_1_repository_1_1_i_repository.html#a9181a13bcbee5bd6cda67cb1c36a69ed", null ],
    [ "UpdateCar", "interface_car_shop_1_1_repository_1_1_i_repository.html#ac861b29c13e5ca67d40e36a04cbe5b45", null ],
    [ "UpdateExtra", "interface_car_shop_1_1_repository_1_1_i_repository.html#aaad2825f8fdcc119319ee8c7f5bb2c6f", null ],
    [ "UpdateModelExtraSwitch", "interface_car_shop_1_1_repository_1_1_i_repository.html#a3f44cba6a54ec0050c8ae2ef1c8b121e", null ]
];