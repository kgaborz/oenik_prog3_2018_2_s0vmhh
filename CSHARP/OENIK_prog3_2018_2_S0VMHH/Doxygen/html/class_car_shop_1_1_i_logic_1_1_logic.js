var class_car_shop_1_1_i_logic_1_1_logic =
[
    [ "Logic", "class_car_shop_1_1_i_logic_1_1_logic.html#a090a127d0c0d254534377d50f5e3b70e", null ],
    [ "AvaragePrice", "class_car_shop_1_1_i_logic_1_1_logic.html#a626d60f73b5e6c1cd7f027b15fb23665", null ],
    [ "CountCategories", "class_car_shop_1_1_i_logic_1_1_logic.html#a4bd0111d1588de43d37a901b9f970a80", null ],
    [ "Create", "class_car_shop_1_1_i_logic_1_1_logic.html#a395c909ea393df8535f14d57ba5acf74", null ],
    [ "Delete", "class_car_shop_1_1_i_logic_1_1_logic.html#a4b0f0b2072534f0bc36962fa98883dd2", null ],
    [ "GetFullPrice", "class_car_shop_1_1_i_logic_1_1_logic.html#ac1a720bb3cb0abd5a8f40b2a24491b76", null ],
    [ "Read", "class_car_shop_1_1_i_logic_1_1_logic.html#a14cc4369573d5939f9e0d496d698f639", null ],
    [ "Update", "class_car_shop_1_1_i_logic_1_1_logic.html#a52d1571ef0b7b99b6192bfa07ed9be3f", null ],
    [ "WebInteraction", "class_car_shop_1_1_i_logic_1_1_logic.html#a6ee330181acfdb963415d4efdd16880e", null ]
];