var interface_car_shop_1_1_i_logic_1_1_i_logic_i =
[
    [ "AvaragePrice", "interface_car_shop_1_1_i_logic_1_1_i_logic_i.html#a92812b11f8954570b15fc9b1365fe31b", null ],
    [ "CountCategories", "interface_car_shop_1_1_i_logic_1_1_i_logic_i.html#aff04ab4a12ebf933e754c2cc199d2288", null ],
    [ "Create", "interface_car_shop_1_1_i_logic_1_1_i_logic_i.html#a8bd87b032e7f6266048c6e4f5f5ea205", null ],
    [ "Delete", "interface_car_shop_1_1_i_logic_1_1_i_logic_i.html#a27d3ebc88dc90adb6e1db4eec52c95ad", null ],
    [ "GetFullPrice", "interface_car_shop_1_1_i_logic_1_1_i_logic_i.html#af4f8b8d6cf1d707e4965a4bcee5c1ced", null ],
    [ "Read", "interface_car_shop_1_1_i_logic_1_1_i_logic_i.html#a5be837a0057889482d04729697e40337", null ],
    [ "Update", "interface_car_shop_1_1_i_logic_1_1_i_logic_i.html#ae989ad3cfda33b25c8259b4d228db65c", null ],
    [ "WebInteraction", "interface_car_shop_1_1_i_logic_1_1_i_logic_i.html#a3c9374aa87485fc3c23b1e105facf6a8", null ]
];