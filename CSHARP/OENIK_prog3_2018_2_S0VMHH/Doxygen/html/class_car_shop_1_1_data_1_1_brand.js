var class_car_shop_1_1_data_1_1_brand =
[
    [ "Brand", "class_car_shop_1_1_data_1_1_brand.html#aa018f36742fd7057c88e165314ef23af", null ],
    [ "Brandname", "class_car_shop_1_1_data_1_1_brand.html#a68d1c0ab618e5a08fd3fa91ae4b42684", null ],
    [ "Cars", "class_car_shop_1_1_data_1_1_brand.html#ac14ab1851947f3af092c3bb88a1eec08", null ],
    [ "Country", "class_car_shop_1_1_data_1_1_brand.html#a9e5688252c59d0d326b0ea740140f7b9", null ],
    [ "Foundation_year", "class_car_shop_1_1_data_1_1_brand.html#affb5584219307c5337e71000cd7ac772", null ],
    [ "ID", "class_car_shop_1_1_data_1_1_brand.html#a7e3ca385c907b9095ab9083f6142dc9e", null ],
    [ "url", "class_car_shop_1_1_data_1_1_brand.html#ab23023dc273cca8f93c9973909d70977", null ],
    [ "Yearly_income", "class_car_shop_1_1_data_1_1_brand.html#ad585c7ea32070e9a18561d23807b9038", null ]
];