//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CarShop.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Car
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Car()
        {
            this.ModelExtraSwitches = new HashSet<ModelExtraSwitch>();
        }
    
        public decimal ID { get; set; }
        public decimal brand_id { get; set; }
        public string name { get; set; }
        public Nullable<decimal> release { get; set; }
        public Nullable<decimal> engine_volume { get; set; }
        public Nullable<decimal> horsepower { get; set; }
        public Nullable<decimal> base_price { get; set; }
    
        public virtual Brand Brand { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ModelExtraSwitch> ModelExtraSwitches { get; set; }
    }
}
