﻿IF OBJECT_ID('Brand', 'U') IS NOT NULL DROP TABLE brand;

IF OBJECT_ID('Cars', 'U') IS NOT NULL DROP TABLE cars;

IF OBJECT_ID('ModelExtraSwitch', 'U') IS NOT NULL DROP TABLE modelExtraSwitch;



CREATE TABLE Brand
 
	(ID             NUMERIC(5) NOT NULL,
	Brandname             VARCHAR(14),

	Country	 VARCHAR(30),
	url		VARCHAR(50),
	Foundation_year	NUMERIC(4),

	Yearly_income	NUMERIC(15),

  CONSTRAINT brand_PRIMARY_KEY PRIMARY KEY (ID));





INSERT INTO Brand VALUES (1,'Volkswagen','Germany','https://wwww.volkswagen.hu/',1937,56000000000);
INSERT INTO Brand VALUES (2,'Mercedes-Benz','Germany','https://www.mercedes-benz.com/hu/',1926,49000000000);
INSERT INTO Brand VALUES (3,'Tesla','USA','https://www.tesla.com/',1909,3700000000);
INSERT INTO Brand VALUES (4,'Audi','Germany','https://wwww.audi.hu/',1932,48000000000);
INSERT INTO Brand VALUES (5,'BMW','Germany','https://www.bmw.hu/hu/index.html',1916,50000000000);

CREATE TABLE Car
 
	(ID             NUMERIC(5) NOT NULL,

	brand_id	NUMERIC(5) NOT NULL,

	name             VARCHAR(14),

	release	NUMERIC(4),

	engine_volume	NUMERIC(10),
	horsepower		NUMERIC(10),

	base_price		NUMERIC(10),

	CONSTRAINT car_FOREIGN_KEY FOREIGN KEY (brand_id) REFERENCES brand (ID),

	CONSTRAINT car_PRIMARY_KEY PRIMARY KEY (ID));

INSERT INTO Car VALUES (1,1,'Golf VII',2012,1000,140,5000000);
INSERT INTO Car VALUES (2,1,'Passat B8',2017,1200,150,12000000);
INSERT INTO Car VALUES (3,2,'CLA 250',2013,1000,360,9000000);
INSERT INTO Car VALUES (4,3,'Tesla Model S',2016,1000,422,15000000);
INSERT INTO Car VALUES (5,3,'Tesla Model 3',2018,1000,700,18000000);
INSERT INTO Car VALUES (6,4,'A3',2010,1000,200,8000000);
INSERT INTO Car VALUES (7,4,'A8',2013,1400,380,20000000);
INSERT INTO Car VALUES (8,5,'BMW X4',2014,800,354,14000000);
INSERT INTO Car VALUES (9,5,'BMW X6',2017,1000,450,18000000);

CREATE TABLE Extra
 
	(ID             NUMERIC(5) NOT NULL,

	category	VARCHAR(14),

	name             VARCHAR(14),

	price		NUMERIC(10),

	colour		VARCHAR(14),
	reusability		BIT NOT NULL DEFAULT(0)
	CONSTRAINT extra_PRIMARY_KEY PRIMARY KEY (id));

INSERT INTO Extra VALUES (1,'accessory','spoiler',50000,'white',1);
INSERT INTO Extra VALUES (2,'accessory','spoiler',50000,'black',1);
INSERT INTO Extra VALUES (3,'accessory','spoiler',50000,'blue',1);
INSERT INTO Extra VALUES (4,'accessory','spoiler',50000,'red',1);
INSERT INTO Extra VALUES (5,'accessory','neon',100000,'white',1);
INSERT INTO Extra VALUES (6,'accessory','neon',100000,'red',1);
INSERT INTO Extra VALUES (7,'accessory','neon',100000,'blue',1);
INSERT INTO Extra VALUES (8,'accessory','neon',100000,'green',1);
INSERT INTO Extra VALUES (9,'painting','striping',30000,'white',0);
INSERT INTO Extra VALUES (10,'painting','striping',30000,'red',0);
INSERT INTO Extra VALUES (11,'painting','striping',30000,'blue',0);




CREATE TABLE ModelExtraSwitch
 
	(ID             NUMERIC(5) NOT NULL,

	car_id	NUMERIC(5) NOT NULL,

	extra_id	NUMERIC(5) NOT NULL,

	CONSTRAINT ModelExtraSwitch_FOREIGN_KEY1 FOREIGN KEY (car_id) REFERENCES car (ID),
	CONSTRAINT ModelExtraSwitch_FOREIGN_KEY2 FOREIGN KEY (extra_id) REFERENCES extra (ID),
	CONSTRAINT ModelExtraSwitch_PRIMARY_KEY PRIMARY KEY (ID));

INSERT INTO ModelExtraSwitch VALUES (1,1,4);
INSERT INTO ModelExtraSwitch VALUES (2,2,5);
INSERT INTO ModelExtraSwitch VALUES (3,3,6);
INSERT INTO ModelExtraSwitch VALUES (4,4,7);
INSERT INTO ModelExtraSwitch VALUES (5,5,8);
INSERT INTO ModelExtraSwitch VALUES (6,6,9);
INSERT INTO ModelExtraSwitch VALUES (7,7,10);


		