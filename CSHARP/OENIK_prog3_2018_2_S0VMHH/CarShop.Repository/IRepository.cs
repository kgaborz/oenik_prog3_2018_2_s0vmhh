﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace CarShop.Repository
{
    using System.Collections.Generic;
    using CarShop.Data;

    /// <summary>
    /// The interface of the Repository, which defines the CRUD methods.
    /// </summary>
    internal interface IRepository
    {
        /// <summary>
        /// Creating a new Brand in Brand table.
        /// </summary>
        /// <param name="data">Data of the new record.</param>
        void CreateBrand(string data);

        /// <summary>
        /// Gets all datas from the Brand table.
        /// </summary>
        void ReadBrand();

        /// <summary>
        /// Updating Brand table with an input.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be changed.</param>
        /// <param name="newinput">The updating parameter.</param>
        void UpdateBrand(string id, string newinput);

        /// <summary>
        /// Deleting a record Brand table.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be deleted.</param>
        void DeleteBrand(string id);

        /// <summary>
        /// Creating a new Car in Car table.
        /// </summary>
        /// <param name="data">Data of the new record.</param>
        void CreateCar(string data);

        /// <summary>
        /// Gets all datas from Car table.
        /// </summary>
        void ReadCar();

        /// <summary>
        /// Updating Car table with an input.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be changed.</param>
        /// <param name="newinput">The updating parameter.</param>
        void UpdateCar(string id, string newinput);

        /// <summary>
        /// Deleting a record from Car table.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be deleted.</param>
        void DeleteCar(string id);

        /// <summary>
        /// Creating a new Extra in Extra table.
        /// </summary>
        /// <param name="data">Data of the new record.</param>
        void CreateExtra(string data);

        /// <summary>
        /// Gets all datas from Extra table.
        /// </summary>
        void ReadExtra();

        /// <summary>
        /// Updating Extra table with an input.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be changed.</param>
        /// <param name="newinput">The updating parameter.</param>
        void UpdateExtra(string id, string newinput);

        /// <summary>
        /// Deleting a record from Extra table.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be deleted.</param>
        void DeleteExtra(string id);

        /// <summary>
        /// Creating a new ModelExtraSwitch in ModelExtraSwitch table.
        /// </summary>
        /// <param name="data">Data of the new record.</param>
        void CreateModelExtraSwitch(string data);

        /// <summary>
        /// Gets all datas from ModelExtraSwitch table.
        /// </summary>
        void ReadModelExtraSwitch();

        /// <summary>
        /// Updating ModelExtraSwitch table with an input.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be changed.</param>
        /// <param name="newinput">The updating parameter.</param>
        void UpdateModelExtraSwitch(string id, string newinput);

        /// <summary>
        /// Deleting a record from ModelExtraSwitch table.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be deleted.</param>
        void DeleteModelExtraSwitch(string id);

        /// <summary>
        /// Counts how many extras are connected to a car from each type of categories.
        /// </summary>
        void CountCategories();

        /// <summary>
        /// Gets a car's full price (Base price + extras).
        /// </summary>
        /// <param name="id">The car's ID which we want to know the full price of.</param>
        /// <returns>Returns the full price of the car.</returns>
        decimal GetFullPrice(string id);

        /// <summary>
        /// Get the avarage price all of the cars from each brands.
        /// </summary>
        /// <returns>Returns the avarage price.</returns>
        IEnumerable<decimal> AvaragePrice();
    }
}
