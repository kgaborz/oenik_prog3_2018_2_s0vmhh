﻿// <copyright file="RepositoryC.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;

    /// <summary>
    /// Repository class, which implements <see cref="IRepository"/> interface.
    /// The class defines the CRUD methods.
    /// </summary>
    public class RepositoryC : IRepository
    {
        private static CarShopDatabaseEntities csEntity = new CarShopDatabaseEntities();

        /// <summary>
        /// Creating a new Brand in Brand table.
        /// </summary>
        /// <param name="data">Data of the new record.</param>
        public void CreateBrand(string data)
        {
            try
            {
                var dataProcess = data.Split(';');
                Brand brand = new Brand()
                {
                    ID = int.Parse(dataProcess[0]),
                    Brandname = dataProcess[1],
                    Country = dataProcess[2],
                    url = dataProcess[3],
                    Foundation_year = int.Parse(dataProcess[4]),
                    Yearly_income = int.Parse(dataProcess[5]),
                };
                csEntity.Brand.Add(brand);
                csEntity.SaveChanges();
                Console.WriteLine("A new brand has been added");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Gets all datas from the Brand table.
        /// </summary>
        public void ReadBrand()
        {
            var maindata = from x in csEntity.Brand select x;
            var dataid = (from x in csEntity.Brand select x.ID.ToString().Length).Max() + 5;
            var dataBN = (from x in csEntity.Brand select x.Brandname.Length).Max() + 5;
            var dataCountry = (from x in csEntity.Brand select x.Country.Length).Max() + 5;
            var dataurl = (from x in csEntity.Brand select x.url.Length).Max() + 5;
            var dataFy = (from x in csEntity.Brand select x.Foundation_year.ToString().Length).Max() + 5;
            var dataYinc = (from x in csEntity.Brand select x.Yearly_income.ToString().Length).Max() + 5;

            foreach (var item in maindata)
            {
                Console.WriteLine(
                    "{0}{1}{2}{3}{4}{5}",
                    item.ID.ToString().PadRight(dataid),
                    item.Brandname.PadRight(dataBN),
                    item.Country.PadRight(dataCountry),
                    item.url.PadRight(dataurl),
                    item.Foundation_year.ToString().PadRight(dataFy),
                    item.Yearly_income.ToString().PadRight(dataYinc));
            }
        }

        /// <summary>
        /// Updating Brand table with an input.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be changed.</param>
        /// <param name="newinput">The updating parameter.</param>
        public void UpdateBrand(string id, string newinput)
        {
            try
            {
                var searchedID = int.Parse(id);
                Brand selectedBrand = csEntity.Brand.Single(x => x.ID == searchedID);
                var fresh = newinput.Split(';');
                selectedBrand.Brandname = fresh[0];
                selectedBrand.Country = fresh[1];
                selectedBrand.url = fresh[2];
                selectedBrand.Foundation_year = int.Parse(fresh[3]);
                selectedBrand.Yearly_income = int.Parse(fresh[4]);
                csEntity.SaveChanges();
                Console.WriteLine("Update was successful!");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Deleting a record Brand table.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be deleted.</param>
        public void DeleteBrand(string id)
        {
            try
            {
                var searchedID = int.Parse(id);
                Brand selectedBrand = csEntity.Brand.Single(x => x.ID == searchedID);
                List<Car> carfkdestroy = csEntity.Car.Where(x => x.brand_id == searchedID).ToList();
                List<ModelExtraSwitch> mesfkdestroy = csEntity.ModelExtraSwitch.Where(x => csEntity.Car.Any(y => y.ID.Equals(x.car_id))).ToList();
                csEntity.Car.RemoveRange(carfkdestroy);
                csEntity.ModelExtraSwitch.RemoveRange(mesfkdestroy);
                csEntity.Brand.Remove(selectedBrand);
                csEntity.SaveChanges();
                Console.WriteLine("Successfully deleted!");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Creating a new Car in Car table.
        /// </summary>
        /// <param name="data">Data of the new record.</param>
        public void CreateCar(string data)
        {
            try
            {
                var dataProcess = data.Split(';');
                Car car = new Car()
                {
                    ID = int.Parse(dataProcess[0]),
                    name = dataProcess[1],
                    release = int.Parse(dataProcess[2]),
                    engine_volume = int.Parse(dataProcess[3]),
                    horsepower = int.Parse(dataProcess[4]),
                    base_price = int.Parse(dataProcess[5]),
                };
                csEntity.Car.Add(car);
                csEntity.SaveChanges();
                Console.WriteLine("A new car has been added");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Gets all datas from Car table.
        /// </summary>
        public void ReadCar()
        {
            var maindata = from x in csEntity.Car select x;
            var dataid = (from x in csEntity.Car select x.ID.ToString().Length).Max() + 5;
            var dataBrandid = (from x in csEntity.Car select x.brand_id.ToString().Length).Max() + 5;
            var dataName = (from x in csEntity.Car select x.name.Length).Max() + 5;
            var dataRelease = (from x in csEntity.Car select x.release.ToString().Length).Max() + 5;
            var dataEV = (from x in csEntity.Car select x.engine_volume.ToString().Length).Max() + 5;
            var dataHP = (from x in csEntity.Car select x.horsepower.ToString().Length).Max() + 5;
            var dataBP = (from x in csEntity.Car select x.base_price.ToString().Length).Max() + 5;

            foreach (var item in maindata)
            {
                Console.WriteLine(
                    "{0}{1}{2}{3}{4}{5}{6}",
                    item.ID.ToString().PadRight(dataid),
                    item.brand_id.ToString().PadRight(dataBrandid),
                    item.name.PadRight(dataName),
                    item.release.ToString().PadRight(dataRelease),
                    item.engine_volume.ToString().PadRight(dataEV),
                    item.horsepower.ToString().PadRight(dataHP),
                    item.base_price.ToString().PadRight(dataBP));
            }
        }

        /// <summary>
        /// Updating Car table with an input.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be changed.</param>
        /// <param name="newinput">The updating parameter.</param>
        public void UpdateCar(string id, string newinput)
        {
            try
            {
                var searchedID = int.Parse(id);
                Car selectedCar = csEntity.Car.Single(x => x.ID == searchedID);
                var fresh = newinput.Split(';');
                selectedCar.name = fresh[0];
                selectedCar.release = int.Parse(fresh[1]);
                selectedCar.engine_volume = int.Parse(fresh[2]);
                selectedCar.horsepower = int.Parse(fresh[3]);
                selectedCar.base_price = int.Parse(fresh[4]);
                csEntity.SaveChanges();
                Console.WriteLine("Update was successful!");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Deleting a record from Car table.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be deleted.</param>
        public void DeleteCar(string id)
        {
            try
            {
                var searchedID = int.Parse(id);
                Car selectedCar = csEntity.Car.Single(x => x.ID == searchedID);
                List<ModelExtraSwitch> fkdestroy = csEntity.ModelExtraSwitch.Where(x => x.car_id == searchedID).ToList();
                csEntity.ModelExtraSwitch.RemoveRange(fkdestroy);
                csEntity.Car.Remove(selectedCar);
                csEntity.SaveChanges();
                Console.WriteLine("Successfully deleted!");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Creating a new Extra in Extra table.
        /// </summary>
        /// <param name="data">Data of the new record.</param>
        public void CreateExtra(string data)
        {
            try
            {
                var dataProcess = data.Split(';');
                Extra extra = new Extra()
                {
                    ID = int.Parse(dataProcess[0]),
                    category = dataProcess[1],
                    name = dataProcess[2],
                    price = int.Parse(dataProcess[3]),
                    colour = dataProcess[4],
                    reusability = bool.Parse(dataProcess[5]),
                };
                csEntity.Extra.Add(extra);
                csEntity.SaveChanges();
                Console.WriteLine("A new extra has been added");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Gets all datas from Extra table.
        /// </summary>
        public void ReadExtra()
        {
            var maindata = from x in csEntity.Extra select x;
            var dataid = (from x in csEntity.Extra select x.ID.ToString().Length).Max() + 5;
            var dataCategory = (from x in csEntity.Extra select x.category.Length).Max() + 5;
            var dataName = (from x in csEntity.Extra select x.name.Length).Max() + 5;
            var dataPrice = (from x in csEntity.Extra select x.price.ToString().Length).Max() + 5;
            var dataColour = (from x in csEntity.Extra select x.colour.Length).Max() + 5;
            var dataReusability = (from x in csEntity.Extra select x.reusability.ToString().Length).Max() + 5;

            foreach (var item in maindata)
            {
                Console.WriteLine(
                    "{0}{1}{2}{3}{4}{5}",
                    item.ID.ToString().PadRight(dataid),
                    item.category.PadRight(dataCategory),
                    item.name.PadRight(dataName),
                    item.price.ToString().PadRight(dataPrice),
                    item.colour.PadRight(dataColour),
                    item.reusability.ToString().PadRight(dataReusability));
            }
        }

        /// <summary>
        /// Updating Extra table with an input.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be changed.</param>
        /// <param name="newinput">The updating parameter.</param>
        public void UpdateExtra(string id, string newinput)
        {
            try
            {
                var searchedID = int.Parse(id);
                Extra selectedExtra = csEntity.Extra.Single(x => x.ID == searchedID);
                var fresh = newinput.Split(';');
                selectedExtra.category = fresh[0];
                selectedExtra.name = fresh[1];
                selectedExtra.price = int.Parse(fresh[2]);
                selectedExtra.colour = fresh[3];
                selectedExtra.reusability = bool.Parse(fresh[4]);
                csEntity.SaveChanges();
                Console.WriteLine("Update was successful!");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Deleting a record from Extra table.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be deleted.</param>
        public void DeleteExtra(string id)
        {
            try
            {
                var searchedID = int.Parse(id);
                Extra selectedExtra = csEntity.Extra.Single(x => x.ID == searchedID);
                List<ModelExtraSwitch> fkdestroy = csEntity.ModelExtraSwitch.Where(x => x.car_id == searchedID).ToList();
                csEntity.ModelExtraSwitch.RemoveRange(fkdestroy);
                csEntity.Extra.Remove(selectedExtra);
                csEntity.SaveChanges();
                Console.WriteLine("Successfully deleted!");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Creating a new ModelExtraSwitch in ModelExtraSwitch table.
        /// </summary>
        /// <param name="data">Data of the new record.</param>
        public void CreateModelExtraSwitch(string data)
        {
            try
            {
                var dataProcess = data.Split(';');
                ModelExtraSwitch mes = new ModelExtraSwitch()
                {
                    ID = int.Parse(dataProcess[0]),
                    car_id = int.Parse(dataProcess[1]),
                    extra_id = int.Parse(dataProcess[2]),
                };
                csEntity.ModelExtraSwitch.Add(mes);
                csEntity.SaveChanges();
                Console.WriteLine("A new ModelExtraSwitch has been added");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Gets all datas from ModelExtraSwitch table.
        /// </summary>
        public void ReadModelExtraSwitch()
        {
            var maindata = from x in csEntity.ModelExtraSwitch select x;
            var dataid = (from x in csEntity.ModelExtraSwitch select x.ID.ToString().Length).Max() + 5;
            var dataCarId = (from x in csEntity.ModelExtraSwitch select x.car_id.ToString().Length).Max() + 5;
            var dataExtraId = (from x in csEntity.ModelExtraSwitch select x.extra_id.ToString().Length).Max() + 5;

            foreach (var item in maindata)
            {
                Console.WriteLine(
                    "{0}{1}{2}",
                    item.ID.ToString().PadRight(dataid),
                    item.car_id.ToString().PadRight(dataCarId),
                    item.extra_id.ToString().PadRight(dataExtraId));
            }
        }

        /// <summary>
        /// Updating ModelExtraSwitch table with an input.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be changed.</param>
        /// <param name="newinput">The updating parameter.</param>
        public void UpdateModelExtraSwitch(string id, string newinput)
        {
            try
            {
                var searchedID = int.Parse(id);
                ModelExtraSwitch selectedExtra = csEntity.ModelExtraSwitch.Single(x => x.ID == searchedID);
                var fresh = newinput.Split(';');
                selectedExtra.car_id = int.Parse(fresh[0]);
                selectedExtra.extra_id = int.Parse(fresh[1]);
                csEntity.SaveChanges();
                Console.WriteLine("Update was successful!");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Deleting a record from ModelExtraSwitch table.
        /// </summary>
        /// <param name="id">ID, which indicates to the data that needs to be deleted.</param>
        public void DeleteModelExtraSwitch(string id)
        {
            try
            {
                var searchedID = int.Parse(id);
                ModelExtraSwitch selectedMES = csEntity.ModelExtraSwitch.Single(x => x.ID == searchedID);
                csEntity.ModelExtraSwitch.Remove(selectedMES);
                csEntity.SaveChanges();
                Console.WriteLine("Successfully deleted!");
            }
            catch (WrongDataException exc)
            {
                Console.WriteLine("Error: invalid data.\n {0}", exc.Message);
            }
            finally
            {
                Console.WriteLine("Some operation has been done.");
            }
        }

        /// <summary>
        /// Counts how many extras are connected to a car from each type of categories.
        /// </summary>
        public void CountCategories()
        {
                var result = (from x in csEntity.ModelExtraSwitch
                    select new
                               {
                                   Category = x.Extra.category,

                                   Accessory = csEntity.ModelExtraSwitch.Where(xtype => xtype.Extra.category == "accessory" && xtype.Extra.category == x.Extra.category).Count(),

                                   Painting = csEntity.ModelExtraSwitch.Where(xtype => xtype.Extra.category == "painting" && xtype.Extra.category == x.Extra.category).Count(),
                               }).Distinct().ToList();
                foreach (var x in result)
            {
                Console.WriteLine(x.Category, x.Accessory, x.Painting);
            }
        }

        /// <summary>
        /// Gets a car's full price (Base price + extras).
        /// </summary>
        /// <param name="id">The car's ID which we want to know the full price of.</param>
        /// <returns>Returns the full price of the car.</returns>
        public decimal GetFullPrice(string id)
        {
            return csEntity.ModelExtraSwitch.Where(x => x.Car.ID == decimal.Parse(id)).Sum(e => (decimal)e.Extra.price) + (decimal)csEntity.Car.Single(x => x.ID == decimal.Parse(id)).base_price;
        }

        /// <summary>
        /// Get the avarage price all of the cars from each brands.
        /// </summary>
        /// <returns>Returns the avarage price.</returns>
        public IEnumerable<decimal> AvaragePrice()
        {
            var maindata = from x in csEntity.Brand select x;
            foreach (var brand in maindata)
            {
                yield return this.BrandAvaragePrice(brand);
            }
        }

        private decimal BrandAvaragePrice(Brand brand)
        {
            List<Car> brandCars = csEntity.Car.Where(x => x.brand_id == brand.ID).ToList();
            return brandCars.Average(x => (decimal)x.base_price);
        }
    }
}
