﻿// <copyright file="WrongDataException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// An Exception class which triggers when there were invalid data.
    /// </summary>
    [Serializable]
    internal class WrongDataException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WrongDataException"/> class.
        /// Exception, which operates when there were invalid data.
        /// </summary>
        /// <param name="message">The ecpetion's message.</param>
        public WrongDataException(string message)
        {
            Console.WriteLine("Error: invalid data.\n {0}", message);
        }
    }
}