/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gabriel
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CarProvider {
    
    @XmlElement
    List<Car> cars;

    public CarProvider(String name, String price){
        cars = new ArrayList<>();
        Random r = new Random();
        cars.add(new Car(name, Integer.parseInt(price) + r.nextInt(10000000) + 500000));
        cars.add(new Car(name, Integer.parseInt(price) + r.nextInt(10000000) + 500000));
        cars.add(new Car(name, Integer.parseInt(price) + r.nextInt(10000000) + 500000));
        cars.add(new Car(name, Integer.parseInt(price) + r.nextInt(10000000) + 500000));
        cars.add(new Car(name, Integer.parseInt(price) + r.nextInt(10000000) + 500000));
    }

    public CarProvider() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Car> getCars() {
        return cars;
    }
    
    
    
    
}
