/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Model.CarProvider;
import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;

/**
 *
 * @author Gabriel
 */
@WebServlet(name ="XMLSender", urlPatterns = {"/XMLSender"})
public class XMLSender extends HttpServlet{
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParserConfigurationException {
        
            response.setContentType("text/xml");  
            Map<String, String[]> map = request.getParameterMap();
                CarProvider cp = new CarProvider(map.get("name")[0], map.get("price")[0]);
              try {
                JAXBContext ctx = JAXBContext.newInstance(CarProvider.class);
                Marshaller marschaller = ctx.createMarshaller();
                marschaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marschaller.marshal(cp, response.getOutputStream());
            } catch (JAXBException e) {
            }  
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/xml");        
                CarProvider cp = new CarProvider(request.getParameter("name"), request.getParameter("price"));
              try {
                JAXBContext ctx = JAXBContext.newInstance(CarProvider.class);
                Marshaller marschaller = ctx.createMarshaller();
                marschaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marschaller.marshal(cp, response.getOutputStream());
            } catch (JAXBException e) {
            }  
              
        }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
  response.setContentType("text/xml");        
                CarProvider cp = new CarProvider(request.getParameter("name"), request.getParameter("price"));
              try {
                JAXBContext ctx = JAXBContext.newInstance(CarProvider.class);
                Marshaller marschaller = ctx.createMarshaller();
                marschaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marschaller.marshal(cp, response.getOutputStream());
            } catch (JAXBException e) {
            }
}
        
}
    
